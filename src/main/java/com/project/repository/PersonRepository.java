package com.project.repository;

import org.springframework.data.repository.CrudRepository;
import com.project.model.Person;

public interface PersonRepository extends CrudRepository<Person, Integer> {
	public Person getById(Integer id);
}