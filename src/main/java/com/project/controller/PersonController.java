package com.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.model.Person;
import com.project.repository.PersonRepository;

@RestController
@RequestMapping("/person")
public class PersonController {
	@Autowired
	private PersonRepository personRepository;
	
	@GetMapping("/list")
	public ResponseEntity<Object> listPerson(){
		return ResponseEntity.status(HttpStatus.OK).body(personRepository.findAll());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> getPerson(@PathVariable Integer id){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(personRepository.getById(id));
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	@PostMapping("/add")
	public ResponseEntity<Object> newPerson(@RequestBody Person person) {
		try {
			personRepository.save(person);
		} catch(Exception ex) {
			throw ex;
		}
		return ResponseEntity.status(HttpStatus.CREATED).body("Success");
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deletePerson(@PathVariable Integer id) {
		try {
			personRepository.deleteById(id);
		} catch(Exception ex) {
			throw ex;
		}
		return ResponseEntity.status(HttpStatus.OK).body("Deleted");
	}
	
	@PostMapping("/update")
	public ResponseEntity<Object> updatePerson(@RequestBody Person person) {
		try {
			personRepository.save(person);
		} catch(Exception ex) {
			throw ex;
		}
		return ResponseEntity.status(HttpStatus.OK).body("Updated");
	}
}
